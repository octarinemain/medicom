$(document).ready(function () {

    //Head Slider
    var swiperHeadScreen = new Swiper('.swiper-head-screen', {
        navigation: {
            nextEl: '.swiper-head-screen .swiper-button-next',
            prevEl: '.swiper-head-screen .swiper-button-prev'
        },
        pagination: {
            el: '.swiper-head-screen .swiper-pagination',
            clickable: true
        }
    });

    //Certificate Slider
    var swiperCertificate = new Swiper('.swiper-certificate', {
        pagination: {
            el: '.swiper-certificate .swiper-pagination',
            clickable: true
        }
    });

    //License Slider
    var swiperLicense = new Swiper('.swiper-license', {
        slidesPerView: 3,
        spaceBetween: 30,
        navigation: {
            nextEl: '.license .swiper-button-next',
            prevEl: '.license .swiper-button-prev'
        },
        breakpoints: {
            840: {
                slidesPerView: 2
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 20
            }
        }
    });

    //Specialists Slider
    var swiperSpecialists = undefined;
    var specialists = $('#specialists').hasClass('swiper-specialists');

    function initSwiperSpecialists() {
        var screenWidth = $(window).outerWidth();
        if ((screenWidth < (480)) && (swiperSpecialists == undefined)) {
            $('.swiper-specialists .swiper-wrapper').removeClass('specialist-noslider');
            $('.swiper-specialists .swiper-slide').removeClass('specialist-card');
            swiperSpecialists = new Swiper('.swiper-specialists', {
                slidesPerView: 1,
                spaceBetween: 20
            });
        }
        else if ((screenWidth > 481) && (swiperSpecialists != undefined)) {
            swiperSpecialists.destroy();
            swiperSpecialists = undefined;
            $('.swiper-specialists .swiper-wrapper').removeAttr('style');
            $('.swiper-specialists .swiper-slide').removeAttr('style');
            $('.swiper-specialists .swiper-wrapper').addClass('specialist-noslider');
            $('.swiper-specialists .swiper-slide').addClass('specialist-card');
        }
    }

    if (specialists) {
        initSwiperSpecialists();
    }

    $(window).resize(function () {
        if (specialists) {
            initSwiperSpecialists();
        }
    });

    //Equipment Slider
    var swiperEquipment = undefined;
    var equipment = $('#equipment').hasClass('swiper-equipment');

    function initSwiperEquipment() {
        var screenWidth = $(window).outerWidth();
        if ((screenWidth < (480)) && (swiperEquipment == undefined)) {
            swiperEquipment = new Swiper('.swiper-equipment', {
                slidesPerView: 2,
                spaceBetween: 10,
                centeredSlides: true,
                pagination: {
                    el: '.swiper-equipment .swiper-pagination',
                    clickable: true
                }
            });
        }
        else if ((screenWidth > 481) && (swiperEquipment != undefined)) {
            swiperEquipment.destroy();
            swiperEquipment = undefined;
            $('.swiper-equipment .swiper-wrapper').removeAttr('style');
            $('.swiper-equipment .swiper-slide').removeAttr('style');
        }
    }

    if (equipment) {
        initSwiperEquipment();
    }

    $(window).resize(function () {
        if (equipment) {
            initSwiperEquipment();
        }
    });

    //Network Slider
    var swiperNetwork = undefined;
    var network = $('#network').hasClass('swiper-network');

    function initSwiperNetwork() {
        var screenWidth = $(window).outerWidth();
        if ((screenWidth < (480)) && (swiperNetwork == undefined)) {
            $('.swiper-network .swiper-wrapper').removeClass('clinics-noslider');
            $('.swiper-network .swiper-slide').removeClass('clinics-card');
            swiperNetwork = new Swiper('.swiper-network', {
                slidesPerView: 1,
                spaceBetween: 10,
                pagination: {
                    el: '.swiper-network .swiper-pagination',
                    clickable: true
                }
            });
        }
        else if ((screenWidth > 481) && (swiperNetwork != undefined)) {
            swiperNetwork.destroy();
            swiperNetwork = undefined;
            $('.swiper-network .swiper-wrapper').removeAttr('style');
            $('.swiper-network .swiper-slide').removeAttr('style');
            $('.swiper-network .swiper-wrapper').addClass('clinics-noslider');
            $('.swiper-network .swiper-slide').addClass('clinics-card');
        }
    }

    if (network) {
        initSwiperNetwork();
    }

    $(window).resize(function () {
        if (network) {
            initSwiperNetwork();
        }
    });

    //DirectionSpec Slider
    var swiperDirectionSpec = undefined;
    var specialistsDirection = $('#direction-spec').hasClass('swiper-direction-spec');

    function initSwiperDirectionSpec() {
        var screenWidth = $(window).outerWidth();
        if ((screenWidth < (480)) && (swiperDirectionSpec == undefined)) {
            $('.swiper-direction-spec .swiper-wrapper').removeClass('direction-spec-noslider');
            $('.swiper-direction-spec .swiper-slide').removeClass('direction-spec-card');
            swiperDirectionSpec = new Swiper('.swiper-direction-spec', {
                slidesPerView: 1,
                spaceBetween: 20

            });
        }
        else if ((screenWidth > 481) && (swiperDirectionSpec != undefined)) {
            swiperDirectionSpec.destroy();
            swiperDirectionSpec = undefined;
            $('.swiper-direction-spec .swiper-wrapper').removeAttr('style');
            $('.swiper-direction-spec .swiper-slide').removeAttr('style');
            $('.swiper-direction-spec .swiper-wrapper').addClass('direction-spec-noslider');
            $('.swiper-direction-spec .swiper-slide').addClass('direction-spec-card');
        }
    }

    if (specialistsDirection) {
        initSwiperDirectionSpec();
    }

    $(window).resize(function () {
        if (specialistsDirection) {
            initSwiperDirectionSpec();
        }
    });

    //Trusted Slider
    var swiperTrusted = new Swiper('.swiper-trusted', {
        slidesPerView: 4,
        spaceBetween: 25,
        navigation: {
            nextEl: '.trusted .swiper-button-next',
            prevEl: '.trusted .swiper-button-prev'
        },
        breakpoints: {
            620: {
                slidesPerView: 3
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 25
            }
        }
    });

    //Write Slider
    var swiperWrite = new Swiper('.swiper-write', {
        slidesPerView: 4,
        spaceBetween: 25,
        navigation: {
            nextEl: '.about-us-write .swiper-button-next',
            prevEl: '.about-us-write .swiper-button-prev'
        },
        breakpoints: {
            620: {
                slidesPerView: 3
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 25
            }
        }
    });

    //Certf Slider
    var swiperCertf = new Swiper('.swiper-certf', {
        slidesPerView: 6,
        spaceBetween: 25,
        navigation: {
            nextEl: '.direction-certificate .swiper-button-next',
            prevEl: '.direction-certificate .swiper-button-prev'
        },
        breakpoints: {
            1280: {
                slidesPerView: 4
            },
            840: {
                slidesPerView: 4,
                spaceBetween: 15
            },
            620: {
                slidesPerView: 4,
                spaceBetween: 15
            },
            480: {
                slidesPerView: 1,
                spaceBetween: 80
            }
        }
    });

    //Reviews Slider
    var swiperReviews = new Swiper('.swiper-reviews', {
        slidesPerView: 1,
        navigation: {
            nextEl: '.reviews .swiper-button-next',
            prevEl: '.reviews .swiper-button-prev'
        },
        pagination: {
            el: '.reviews .swiper-pagination',
            clickable: true
        },
        breakpoints: {
            840: {
                spaceBetween: 20
            }
        }
    });

    // Medic Card text
    $(".specialist-prof p").html(function (index, currentText) {
        if ($(this).text().length <= 65) {
            return currentText
        } else {
            return currentText.substr(0, 65) + "...";
        }
    });

    // Mask Phone
    if ($('input').hasClass('js-mask-phone')) {
        $(".js-mask-phone").mask("+38 (999) 999 99 99");
    }

    //Review Select
    $('.rating-select').on('click', 'img', function () {
        var rewActive = '<img src="img/ok-star.svg" alt="">';
        var rewDefault = '<img src="img/no-star.svg" alt="">';
        $(this).prevAll().replaceWith(rewActive);
        $(this).nextAll().replaceWith(rewDefault);
        $(this).replaceWith(rewActive);
    });

    // Accordion
    if ($('div').hasClass('accordion')) {
        $('.accordion').accordion({
            "transitionSpeed": 400
        });
    }

    // Anchor link
    $(".nav-for-page ul").on("click", "a", function (e) {
        // e.preventDefault();
        $(".nav-for-page li").removeClass('active');
        $(this).closest('li').addClass('active');
        var id = $(this).attr('href'),
            top = $(id).offset().top - 72;
        $('body, html').animate({scrollTop: top}, 1500);
    });

    // Fancybox Slider
    if ($('a').hasClass('fancybox')) {
        $(".fancybox").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            nextEffect: 'none',
            prevEffect: 'none',
            padding: [7, 7, 7, 7]
        });
    }

    if ($('a').hasClass('video')) {
        $(".video").fancybox({
            openEffect: 'none',
            closeEffect: 'none',
            nextEffect: 'none',
            prevEffect: 'none',
            height: '500',
            helpers: {
                media: {}
            },
            padding: [7, 7, 7, 7]
        });
    }

    // show Search header
    function showSearch() {

        $('.search-header.desc-s .search-icon').on('click', function () {
            var screenWidth = $(window).outerWidth();
            var input = $(this).closest('.search-header.desc-s').find('.search-inp-wrap');
            var logoImg = $('#header-logo');
            var entry = $('#entry');

            $('.nav-link').hide("slow");
            $('#sm-menu').removeClass("active");
            input.show("slow");
            logoImg.css("display", 'none');
            entry.css("display", 'none');

            if (screenWidth <= 621) {
                entry.css("display", 'none');
                $('.call-info').css("display", 'none');
            }

            if (screenWidth >= 808) {
                entry.css("display", 'block');
            }

            if (screenWidth > 840) {
                $(this).addClass('active');
            } else {
                $(this).removeClass('active');
            }

            $(document).on('click', function (e) {
                var div = $(".search-header.desc-s");

                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.search-icon').removeClass('active');
                    $('.nav-link').show("slow");
                    brg();
                    div.find('.search-inp-wrap').hide("slow");

                    logoImg.css("display", 'block');
                    entry.css("display", 'block');
                    $('.call-info').css("display", 'block');
                    $('#search-price, #search-prices').val('');
                    $('.drop-result').removeClass('active');
                }
            });
        });
    }

    $(window).on('load resize', function () {
        showSearch();
    });

    // Search Price--------------------------------------------------------------------------
    $('#search-price, #search-prices').bind('keyup', function () {
        var searchTerm = $(this).val();
        var searchSelect = $('.drop-result');

        searchSelect.removeHighlight();

        if (searchTerm.length >= 3) {
            searchSelect.addClass('active');

            if (searchTerm) {
                searchSelect.highlight(searchTerm);
            }

        } else {
            searchSelect.removeClass('active');
        }

        if (!$('#search-price, #search-prices').is(":focus")) {
            $(this).val('');
            searchSelect.removeClass('active');
        }

        if (searchTerm.length >= 1) {
            $('.js-clearVal').addClass('active');
        } else {
            $('.js-clearVal').removeClass('active');
        }
    });

    // Show Drop Result
    $('.drop-result').on('click', 'a', function (e) {
        e.preventDefault();
        var resultText = $(this).text();
        $(this).closest('.search-inp-wrap').find('input').val(resultText);
        $(this).closest('.drop-result').removeClass('active');
    });

    // Clear btn
    $('.js-clearVal').on('click', function () {
        $(this).closest('.search-inp-wrap').find('#search-price').val('');
        $('#search-price').val('');
        $('.drop-result').removeClass('active');
    });
    // Search Price End-----------------------------------------------------------------------

    // More info services
    $('.services .services-desc').each(function () {
        if ($(this).height() > 85) {
            $(this).closest('.services').addClass('active');
        }
    });

    $('.services .more-services-info').on('click', function () {
        var desc = $(this).closest('.services').find('.services-desc');
        desc.toggleClass('active');
        $(this).toggleClass('active');
        $('.accordion').trigger("resize");
    });

    $('.js-open-rev').on('click', function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        $('.send-review').toggleClass('active');
    });

    // Download -------------------------------
    $('.download').click(function (e) {
        e.preventDefault();
        var img = new Image;
        img.onload = function () {
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0);

            var base64Image = getBase64Image(canvas);
            downloadURI(base64Image, 'license.png');
        };
        img.setAttribute('crossOrigin', 'anonymous');
        var urlImg = $(this).attr('href');
        img.src = urlImg;
    });

    function getBase64Image(canvas) {
        var dataURL = canvas.toDataURL("image/png");
        return dataURL;
    }

    function downloadURI(uri, name) {
        // IE10+ : (has Blob, but not a[download] or URL)
        if (navigator.msSaveBlob) {
            const blob = dataURItoBlob(uri);
            return navigator.msSaveBlob(blob, name);
        }
        const link = document.createElement('a');
        link.download = name;
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }

    function dataURItoBlob(dataurl) {
        const parts = dataurl.split(','), mime = parts[0].match(/:(.*?);/)[1];
        if (parts[0].indexOf('base64') !== -1) {
            const bstr = atob(parts[1]);
            var n = bstr.length;
            const u8arr = new Uint8Array(n);

            while (n--) {
                u8arr[n] = bstr.charCodeAt(n)
            }
            return new Blob([u8arr], {type: mime})
        } else {
            const raw = decodeURIComponent(parts[1])
            return new Blob([raw], {type: mime})
        }
    }

    // Download End-------------------------------

    // Card show more
    $('.feedback-desc').each(function () {
        if ($(this).height() > 70) {
            $(this).closest('.feedback-content').addClass('active');
        }
    });

    $('.feedback-content .more-services-info').on('click', function () {
        $(this).toggleClass('active');
        var desc = $(this).closest('.feedback-content').find('.feedback-desc');
        desc.toggleClass('active');
    });

    // File
    function resumeFile() {
        var fileInput = document.querySelector('.form-input-file');
        var fileInputText = document.querySelector('.form-input--file-text');
        var formInpWrap = document.querySelector('.form-input-wrap');
        var exit = document.querySelector('.exit-file');
        var fileInputTextContent = fileInputText.textContent;

        fileInput.addEventListener('change', function (e) {
            var value = e.target.value.length > 0 ? e.target.value : fileInputTextContent;
            formInpWrap.classList.add('active');
            exit.classList.add('active');
            fileInputText.textContent = value.replace('C:\\fakepath\\', '');
        });

        $('.exit-file').on('click', function () {
            $(this).removeClass('active');
            $('.form-input-file').val('');
            $('.form-input-wrap').removeClass('active');
            $('.form-input--file-text').text('Загрузить резюме');
        });
    }

    if ($('input').hasClass('form-input-file')) {
        resumeFile();
    }

    // reviews show more
    $('.rev-photo-wrap .more-services-info').on('click', function () {
        $(this).toggleClass('active');
        var photo = $(this).closest('.rev-photo-wrap').find('.rev-photo');
        photo.toggleClass('active');
    });

    // PopUp
    $('.js-popup-entry').on('click', function () {
        popupSearchON();
    });

    $('.exit-pop').on('click', function () {
        $(this).closest('.popup').removeClass('active');
        $('body').removeClass('active');
    });

    function popupSearchON() {
        $('.popup-search').toggleClass('active');
        $('body').toggleClass('active');
        $('.popup-search').on('click', function (e) {
            var div = $(".popup-search > div");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup-search').toggleClass('active');
                $('body').toggleClass('active');
            }
        });
    }

    // Timer
    function timerOver() {
        var counter = 30;

        setInterval(function () {
            counter--;
            if (counter >= 1) {
                $('.time-sec').text(counter);

                if (counter <= 9) {
                    $('.time-sec').text('0' + counter);
                }
            }

            if (counter === 0) {
                $('.time-sec').text('00');
                clearInterval(counter);
            }

        }, 1000);
    }

    // Dropdown phone
    function dropdownPhone() {
        var screenWidth = $(window).outerWidth();

        if (screenWidth <= 620) {
            $('#dropdown-phone').removeClass('dropdown-anchor-top-center');
            $('#dropdown-phone').addClass('dropdown-anchor-top-right');
            $('#dropdown-phone').attr('data-add-y', '20');
        }

        if (screenWidth >= 621) {
            $('#dropdown-phone').removeClass('dropdown-anchor-top-right');
            $('#dropdown-phone').addClass('dropdown-anchor-top-center');
            $('#dropdown-phone').removeAttr('data-add-y');
        }
    }

    dropdownPhone();

    $(window).resize(function () {
        dropdownPhone();
    });

    // Mob append
    $(window).on('load resize', function () {

        $('.wr-l').on('click', function () {
            $('.js-find').removeClass('active');
            $('#search-price, #search-prices').val('');
            $('.drop-result').removeClass('active');
            if (!$(this).closest('.text-link').hasClass('ar')) {
                $('.text-link').removeClass('ar');
                $('.nav-link').removeClass('active');
            }
            $(this).closest('.text-link').toggleClass('ar');
            $(this).closest('.text-link').find('.name-nav').toggleClass('active');

            if ($(window).outerWidth() > 840) {
                if ($(this).closest('.text-link').find('.name-nav').hasClass('active')) {
                    $(this).closest('.nav-link').find('.dropdown-menu').css('display', 'block');
                } else {
                    $(this).closest('.nav-link').find('.dropdown-menu').css('display', 'none');
                }
            }

            if ($(window).outerWidth() > 840) {
                $(document).click(function (e) {
                    var div = $(".dropdown-menu");
                    if (!div.is(e.target) && div.has(e.target).length === 0) {
                        $('.text-link').removeClass('ar');
                        $('.nav-link').find('.dropdown-menu').css('display', 'none');
                        $('.nav-link').removeClass('active');
                    }
                });
            }
        });
        setInterval(navShow, 500);
        function navShow() {
            $('.wrapper-nav-link .nav-link').each(function () {
                var screenWidth = $(window).outerWidth();
                var wraperWidth = $('.wrapper-nav-link').width();
                var coefficient = screenWidth / wraperWidth;
                var resultPersent = 100 / coefficient;

                if (screenWidth < 1000 && screenWidth > 840) {
                    $('.nav-link').removeClass('show');
                    $('.text-link .name-nav').on('click', function () {
                        $(this).closest('.nav-link').addClass('show');
                    });
                    if (resultPersent > 35) {
                        $('.wrapper-nav-link .nav-link:last-child').appendTo('.burger-desk');
                    } else if (resultPersent < 30) {
                        $('.burger-desk .nav-link:last-child').appendTo('.wrapper-nav-link');
                    }
                } else if (screenWidth < 840) {
                    $('.burger-desk .nav-link').appendTo('.wrapper-nav-link');
                } else if (screenWidth > 1000) {
                    if (resultPersent > 49) {
                        $('.wrapper-nav-link .nav-link:last-child').appendTo('.burger-desk');
                    } else if (resultPersent < 42) {
                        $('.burger-desk .nav-link:last-child').appendTo('.wrapper-nav-link');
                    }
                }
            });
            brg();
        }

        brg();
    });

    function brg() {
        if ($('.burger-desk > *').is('.nav-link')) {
            $('#sm-menu').addClass('active');
        } else {
            $('#sm-menu').removeClass('active');
        }
    }

    $('#sm-menu .burger').on('click', function () {
        $(this).toggleClass('active');
        if ($(this).hasClass('active')) {
            $('#sm-menu .dropdown-menu').css('display', 'block');
        } else {
            $('#sm-menu .dropdown-menu').css('display', 'none');
        }
    });

    $('.wcall').on('click', function () {
        $('.text-link').removeClass('ar');
        $('.nav-link').removeClass('active');
        $('.dropdown-menu').css('display', 'none');

        if (!$(this).closest('.js-find').hasClass('active')) {
            $('.js-find').removeClass('active');
        }

        $(this).closest('.js-find').toggleClass('active');

        if ($(this).closest('.js-find').hasClass('active')) {
            $(this).closest('.js-par').find('.dropdown-menu').css('display', 'block');
        } else {
            $(this).closest('.js-par').find('.dropdown-menu').css('display', 'none');
        }

        $(document).click(function (e) {
            var div = $(".dropdown-menu");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.js-find').removeClass('active');
            }
        });
    });

    //duble-menu
    $('.drop-duble').on('click', function () {
        $(this).toggleClass('active');
    });

    $('.nav-link .text-link').on('click', function () {
        $(this).closest('.nav-link').toggleClass('active');
    });

    //Accordion menu in gumburger
    $(window).on('load resize', function () {
        var screenWidth = $(window).outerWidth();

        if (screenWidth <= 840) {
            $('.nav-link .text-link').sweetDropdown('disable');
            $('.nav-link .man-drop').attr('class', 'man-drop');
            $('.nav-link .tabs').attr('class', 'tab');
            $('.nav-link .tabs-child').attr('class', 'tab-ch');
            $('.wrapper-tabs .accordion-end').each(function (i) {
                $('.nav-content .accordion-item').eq(i).after($(this));
            });
        } else if (screenWidth > 840) {
            $('.nav-link .tab').attr('class', 'tabs');
            $('.nav-link .tab-ch').attr('class', 'tabs-child');
            $('.nav-content .accordion-end').appendTo('.wrapper-tabs');
            $('.nav-link .man-drop').attr('class', 'man-drop dropdown-menu dropdown-anchor-top-left dropdown-has-anchor dropdown-opened');
            $('.nav-link .text-link').sweetDropdown('enable');
        }
    });

    // tab-mob
    $('.nav-content .accordion-item').on('click', function () {
        $(this).next().toggleClass('active');
    });

    // Gamburger-menu
    $('#xs-menu').click(function () {
        var navBg = $('.nav-bg').css('display', 'block');

        $('.nav-bg').fadeIn();

        if (navBg) {
            $('header').css('position', 'absolute');
            $('html, body').animate({'scrollTop': '0'}, 500);

        } else {
            $('header').css('position', 'fixed');
        }
    });

    $('.nav-close').click(function () {
        $('.nav-bg').fadeOut();
        $('header').css('position', 'fixed');
    });

    // Gamburger-menu Media > 841px
    // $('#accordion-btn').click(function() {
    //     var arrow = $('#accordion-arrow');
    //     var accordionContent = $('#ul-accordion-content');
    //
    //     accordionContent.slideToggle();
    //     arrow.toggleClass('arr-up-w');
    // });

    // Sweet dropdown > 841px
    function sweetSm() {
        var screenWidth = $(window).outerWidth();

        if (screenWidth >= 841) {
            $('#dropdown-man').attr('data-add-x', '-132');
        }

        if (screenWidth >= 1280) {
            $('#dropdown-man').removeAttr('data-add-x', '-132');
        }
    }

    sweetSm();

    $(window).resize(function () {
        sweetSm();
    });

    $('.text-link').on('click', function () {
        $('html, body').animate({'scrollTop': '0'}, 500);
    });

    // $(document).on('click', function (e) {
    //     var element = $('.man-wrap');
    //
    //     if (!element.is(e.target) && element.has(e.target).length === 0) {
    //         $('header').removeClass('header-rel');
    //     }
    // });

    // footer dropdown
    function contactsDropdown() {
        var screenWidth = $(window).outerWidth();
        var address = $('.contacts-card > .card-address');
        var addressContent = $('.contacts-card > .wrapper-address');

        if (screenWidth <= 480) {
            address.on('click', function () {
                addressContent.not($(this).next()).css('display', 'none');
                $(this).next().css('display', 'block');
            });
        }
        else if (screenWidth >= 481) {
            addressContent.removeAttr('style');
            address.unbind('click');
        }
    }

    contactsDropdown();

    $(window).resize(function () {
        contactsDropdown();
    });

    // nav-for-page left menu
    $('.nav-for-page-text').click(function () {
        $('.nav-for-page-wrap').fadeIn();
        $('body').addClass('active');

        $('.nav-for-page-wrap a').on('click', function () {
            $('body').removeClass('active');
            $('.nav-for-page-wrap').fadeOut();
        });

        $(document).on('click', function (e) {
            var element = $('.nav-for-page');

            if (!element.is(e.target) && element.has(e.target).length === 0) {
                $('.nav-for-page-wrap').fadeOut();
                $('body').removeClass('active');
            }
        });
    });

    $('.nav-for-page-close').click(function () {
        $('.nav-for-page-wrap').fadeOut();
        $('body').removeClass('active');
    });

    // scroll-for-pages
    var scrollClass = $('#navigation-menu').hasClass('nav-for-page');

    function scrollPages() {
        var topNavPage = $('#navigation-menu').offset().top;
        var heightNavPage = $('#navigation-menu').outerHeight();
        var heightBlockReviews = $('#doctors').offset().top;


        $(window).scroll(function () {
            var top = $(document).scrollTop();

            if (top > topNavPage && top < heightBlockReviews - heightNavPage) {
                $('.nav-for-page').fadeIn('fast');
            }

            else if (top > heightBlockReviews - heightNavPage) {
                $('.nav-for-page').fadeOut('fast');
            }

        });
    }

    $('.doctors-result-cards .doc-card').each(function (i) {
        if (i <= 18) {
            $('.doctors-result-cards .doc-card').eq(i).addClass('active');
        }
    });

    // Doc Search show more
    $('.doctors-result-cards .doc-card-more').on('click', function () {
        var lenght = $('.doctors-result-cards .doc-card').length;
        var lenghtActive = $('.doctors-result-cards .doc-card.active').length;

        if (lenght > lenghtActive) {
            $('.doctors-result-cards .doc-card').each(function (i) {
                if (i <= (lenghtActive + 19) && !$(this).hasClass('active')) {
                    $(this).addClass('active');
                }
            });
        }
        lenghtActive = $('.doctors-result-cards .doc-card.active').length;
        if (lenght === lenghtActive) {
            $('.doctors-result-cards .doc-card-more').addClass('hide');
        }
    });

    $('.reviews-doctors .reviews-people').each(function (i) {
        if (i <= 1) {
            $('.reviews-people').eq(i).addClass('js-show-card');
        }
    });

    $('.pages-rev .reviews-people').each(function (i) {
        if (i <= 6) {
            $('.reviews-people').eq(i).addClass('js-show-card');
        }
    });

    $('.feedback-out-staff .js-def-card').each(function (i) {
        if (i <= 4) {
            $('.js-def-card').eq(i).addClass('js-show-card');
        }
    });

    $('.news .js-def-card').each(function (i) {
        if (i <= 1) {
            $('.js-def-card').eq(i).addClass('js-show-card');
        }
    });

    // review show more
    $('.js-btn-more').on('click', function () {
        var lenght = $('.js-def-card').length;
        var lenghtActive = $('.js-def-card.js-show-card').length;

        if (lenght > lenghtActive) {
            $('.js-def-card').each(function (i) {
                if (i <= (lenghtActive + 9) && !$(this).hasClass('js-show-card')) {
                    $(this).addClass('js-show-card');
                }
            });
        }

        lenghtActive = $('.js-def-card.js-show-card').length;

        if (lenght === lenghtActive) {
            $('.js-btn-more').addClass('hide');
        }
    });

    if (scrollClass) {
        scrollPages();
    }

    $(window).scroll(function () {
        if ( $(window).scrollTop() > 20) {
            $('.page-up').addClass('active');
        } else {
            $('.page-up').removeClass('active');
        }
    });

    $('.page-up').on('click', function () {
        $('html, body').animate({scrollTop: 0}, 1000);
    });

    // tabs
    $('.tabs .nav-content li').on('click', function () {
        var i = $(this).index();
        $('.tabs .nav-content li').removeClass('active')
        $(this).addClass('active');
        $('.tabs .wrapper-tabs .accordion-end').css('display', 'none');
        $('.tabs .wrapper-tabs .accordion-end').eq(i).css('display', 'block');
    });

    $('.tabs-child .nav-content li').on('click', function () {
        var i = $(this).index();
        $('.tabs-child .nav-content li').removeClass('active')
        $(this).addClass('active');
        $('.tabs-child .wrapper-tabs .accordion-end').css('display', 'none');
        $('.tabs-child .wrapper-tabs .accordion-end').eq(i).css('display', 'block');
    });
});